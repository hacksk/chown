Format: 3.0 (quilt)
Source: node-chownr
Binary: node-chownr
Architecture: all
Version: 1.0.1-1
Maintainer: Debian Javascript Maintainers <pkg-javascript-devel@lists.alioth.debian.org>
Uploaders: hacksk <kiranskunjumon80@gmail.com>
Homepage: https://github.com/isaacs/chownr#readme
Standards-Version: 3.9.8
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-javascript/node-chownr.git
Vcs-Git: https://anonscm.debian.org/git/pkg-javascript/node-chownr.git
Testsuite: autopkgtest
Build-Depends: debhelper (>= 9), dh-buildinfo, nodejs
Package-List:
 node-chownr deb web optional arch=all
Checksums-Sha1:
 13d1a7fdbca635c8018588ca92fdd0526740fa68 2739 node-chownr_1.0.1.orig.tar.gz
 bb83b4b070c6d4c6eb351123a9f972a308886326 1896 node-chownr_1.0.1-1.debian.tar.xz
Checksums-Sha256:
 429b8c9803124942abfe5d1183514dc2dbadd7be792862457e7c3fc74e1a0a22 2739 node-chownr_1.0.1.orig.tar.gz
 349102fc045a0bc58cdf5b7584cb3f245761de73b0288488f271125768134b60 1896 node-chownr_1.0.1-1.debian.tar.xz
Files:
 76c02b269d8ae9304d2f162958d7636e 2739 node-chownr_1.0.1.orig.tar.gz
 920499e7bfd399e46ee5a123eb601413 1896 node-chownr_1.0.1-1.debian.tar.xz
